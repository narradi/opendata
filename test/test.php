<!doctype html>
<html lang='fr'>
<head>

  <meta charset='utf-8'>
  <title>Trouvez votre formation</title>

<link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,900,700' rel='stylesheet' type='text/css'>
<link href='https://cdn.materialdesignicons.com/1.5.54/css/materialdesignicons.min.css' rel='stylesheet' type='text/css'>
<link href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' rel='stylesheet' type='text/css'>
<link href='site.css' rel='stylesheet' type='text/css'>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='site.js'></script>
<meta name='viewport' content='width=device-width, initial-scale=1'>


</head>

<body>

<form action='research.php' method='get'>




<h1 class='text-center'>
	Trouvez votre formation
	<sup class='text-center' id='date'>
<script>
n =  new Date();
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();
document.getElementById('date').innerHTML = d + '/' + m + '/' + y;
</script> </sup>
</h1>

<div class='container'>
	<section>
		<span text='Votre recherche'></span>

	<div class='row'>
      <div class='half'>
        <div class='input-group'>
          <label>Formation</label>
          <select name='type_diplome[]' class='selectbox-search' multiple>
              <?php

    $json = file_get_contents('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&sort=-rentree_lib&facet=diplome_rgp&refine.rentree_lib=2017-18');
    $parsed_json = json_decode($json, true);
    $var = $parsed_json['facet_groups'][0]['facets'];
    foreach ($var as $value)
    {
        echo '<OPTION value=\'';
        echo $value['name'];
        echo '\'>';
        echo $value['name'];
        echo '</OPTION>';
    }
				?>
          </select>
        </div>
      </div>
      <div class='half'>
        <div class='input-group'>
          <label>Secteur d'activité</label>
          <select name='secteur_disciplinaire[]' class='selectbox-search' multiple>
             <?php

    $json = file_get_contents('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&facet=sect_disciplinaire_lib&refine.rentree_lib=2017-18');
    $parsed_json = json_decode($json, true);
    $var = $parsed_json['facet_groups'][0]['facets'];

    foreach ($var as $value)
    {
        echo '<OPTION value=\'';
        echo $value['name'];
        echo '\'>';
        echo $value['name'];
        echo '</OPTION>';
    }
?>
          </select>
        </div>
      </div>
    </div>
    

      <div class='half'>
        <div class='input-group'>
          <label>Lieu</label>
          <select name='region[]' class='selectbox-search' multiple>
              <?php
    //https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=1&sort=-rentree_lib&facet=reg_ins_lib URL de la vrai BDD
    $json = file_get_contents('https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=5000&sort=-rentree_lib&facet=reg_ins_lib');
    $parsed_json = json_decode($json, true);
    $var = $parsed_json['facet_groups'][0]['facets'];
    foreach ($var as $value)
    {
        echo '<OPTION value=\'';
        echo $value['name'];
        echo '\'>';
        echo $value['name'];
        echo '</OPTION>';
    }
?>
          </select>
        </div>
      </div>

	<div class='button-effect'>
		<a href='research.php'>
			<input class='effect effect-1' type='submit' ></input>
		</a>	
    </div>
    
    </div>
    
    </div>
	</section>
</div>


<div class="footer">
  <p>ARRADI Naoufal</p>
</div>

</form>


</body>

</html>
