<?php
header('content-type: text/html; charset=utf-8');

function initrequest(){

$url   = file_get_contents("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=5000&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.rentree_lib=2017-18");
$url2  = file_get_contents("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=5000&sort=uo_lib&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement");
$json2 = json_decode($url2, true);
$json  = json_decode($url, true);


$formation = isset($_GET['type_diplome']) ? $_GET['type_diplome'] : NULL;


$secteur = isset($_GET['secteur_disciplinaire']) ? $_GET['secteur_disciplinaire'] : NULL;


$region = isset($_GET['region']) ? $_GET['region'] : NULL;


$i     = 0;
$j     = 0;
$local = array();

foreach ($json['records'] as $item) {
    
    $datas              = $item['fields'];
    $diplome_data       = $datas['diplome_rgp'];
    $formation_data     = $datas['libelle_intitule_1'];
    $etablissement_data = $datas['etablissement_lib'];
    $region_data        = $datas['reg_ins_lib'];
    $secteur_data       = $datas['sect_disciplinaire_lib'];
    
    if ($formation == NULL || in_array($diplome_data, $formation)) {
        if ($secteur == NULL || in_array($secteur_data, $secteur)) {
            if ($region == NULL || in_array($region_data, $region)) {
                echo "<tr><td>$diplome_data</td><td>$formation_data</td><td>$etablissement_data</td><td>$region_data</td></tr>";
                $local[] = $item;
                
            }
        }
    }
    
    
}
echo "<script>";

foreach ($local as $ecole) {
    
    foreach ($json2['records'] as $etab) {
        
        if (isset($etab['fields']['uai']) && $etab['fields']['uai'] == $ecole['fields']['etablissement']) {
            if (isset($etab['fields']['coordonnees'])) {
                echo "var m = L.marker([" . $etab['fields']['coordonnees'][0] . "," . $etab['fields']['coordonnees'][1] . "]).addTo(mymap);";
                echo "m.bindPopup(\"<b>" . $ecole['fields']['etablissement_lib'] . "</b>\");";
            }
        }
    }
}
echo "</script>";
}
?>

<!doctype html>
<html lang="fr">
<head>
    <title>Trouvez votre formation</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,100,900,700' rel='stylesheet' type='text/css'>
    <link href="https://cdn.materialdesignicons.com/1.5.54/css/materialdesignicons.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
            integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
            crossorigin=""></script>
    <link href="site.css" rel='stylesheet' type='text/css'>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="site.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>


<div class='button-effect'>
    <button class="button button4" onclick="window.location.href = 'test.php';">Pr&eacute;c&eacute;dent</button>
    </div>
    



<h1 class="text-center">
    Trouvez votre formation
    <sup class="text-center" id="date">
        <script>
            n = new Date();
            y = n.getFullYear();
            m = n.getMonth() + 1;
            d = n.getDate();
            document.getElementById("date").innerHTML = d + "/" + m + "/" + y;
        </script>
    </sup>
</h1>

<div class="container">
    <section>
        <span text="R&eacute;sultat"></span>

        <br/>
        <br/>
        <div id="mapid" style="width: 850px; height: 350px;"></div>
        <script>

            var mymap = L.map('mapid').setView([46.887338, 2.361595], 5);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery � <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox/streets-v11'
            }).addTo(mymap);

        </script>

        <br/>
        <br/>
        <table>

            <tr>
                <th>Dipl&ocirc;me</th>
                <th>Formation</th>
                <th>&Eacute;tablissement</th>
                <th>R&eacute;gion</th>
            </tr>

            <?php

initrequest();

?>

        </table>

    </section>
</div>

</body>


</html>